<?php
require_once 'ConnexionBaseDeDonnees.php';
require_once 'Trajet.php';
class Utilisateur {

    private string $login;
    private string $nom;
    private string $prenom;
    /**
     * @var Trajet[]|null
     */
    private ?array $trajetsCommePassager;

    // un getter
    public function getNom() : string {
        return $this->nom;
    }

    // un setter
    public function setNom($nom) : void {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getPrenom() : string
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom($prenom): void
    {
        $this->prenom = $prenom;
    }

    /**
     * @return mixed
     */
    public function getLogin() : string
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin($login): void
    {
        if (strlen($login) > 64) {
            $this->login = substr($login, 0, 64);
        }else{
            $this->login = $login;
        }
    }

    // un constructeur
    public function __construct(
      string $login,
      string $nom,
      string $prenom,
   ) {
        if (strlen($login) > 64) {
            $this->login = substr($login, 0, 65);
        }else{
            $this->login = $login;
        }
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->trajetsCommePassager = null;
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
    public function __toString() : string {
      // À compléter dans le prochain exercicere
        return "Utilisateur {$this->prenom} {$this->nom} de login {$this->login}";
    }
    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau) : Utilisateur {
        return new Utilisateur($utilisateurFormatTableau[0],$utilisateurFormatTableau[1],$utilisateurFormatTableau[2]);
    }
    public static function recupererUtilisateurs() : array {
        $Pdo = ConnexionBaseDeDonnees::getPdo();
        $sql = 'SELECT * FROM utilisateur';
        $pdoStatement = $Pdo->query($sql);
        $TabUtilisateurs = [];
        foreach($pdoStatement as $utilisateurFormatTableau){
            $utilisateur = Utilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
            $TabUtilisateurs[] = $utilisateur;
        }
        return $TabUtilisateurs;
    }
    public static function recupererUtilisateurParLogin(string $login) : ?Utilisateur {
        $sql = "SELECT * from utilisateur WHERE loginBaseDeDonnees = :loginTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $utilisateurFormatTableau = $pdoStatement->fetch();
        if (empty($utilisateurFormatTableau)){
            return null;
        }else{
            return Utilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
        }
    }
    public function ajouter() : void {
        $sql = "INSERT INTO utilisateur (loginBaseDeDonnees,nomBaseDeDonnees,prenomBaseDeDonnees) VALUES (:login,:nom,:prenom)";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
                "login" => $this->login,
                "nom" => $this->nom,
                "prenom" => $this->prenom,
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);
    }

    /**
     * @return Trajet[]
     */
    private function recupererTrajetsCommePassager() : array{
        $sql = "SELECT * FROM trajet
                WHERE id IN (
                    SELECT trajetId FROM passager
                    WHERE passagerLogin = :loginUtilisateur
                );";
        $trajets = [];
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "loginUtilisateur" => $this->login,
        );
        $pdoStatement->execute($values);
        foreach($pdoStatement as $trajet){
            $trajets[] = Trajet::construireDepuisTableauSQL($trajet);
        }
        return $trajets;
    }

    public function getTrajetsCommePassager(): ?array
    {
        if(empty($this->trajetsCommePassager)){
            return $this->recupererTrajetsCommePassager();
        }else{
            return $this->trajetsCommePassager;
        }
    }

    public function setTrajetsCommePassager(?array $trajetsCommePassager): void
    {
        $this->trajetsCommePassager = $trajetsCommePassager;
    }

}
/*$utilisateur1 = new Utilisateur("rabesonk","Rabeson","Khylian");
$utilisateur2 = new Utilisateur("Leschatssontdesanimauxdomestiquestrèspopulairesdanslemondeentieraujourd'hui","longueurLogin","Test");*/
?>

<!--<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title> Utilisateur.php </title>
    </head>

    <body>
        <h3>Voici le résultat</h3>
        <p><?php
/*            echo $utilisateur1;
        */?></p>
        <p><?php
/*            echo "la phrase de longeur > 64 : Leschatssontdesanimauxdomestiquestrèspopulairesdanslemondeentieraujourd'hui";
            echo "<br>";
            echo $utilisateur2;
        */?></p>
    </body>
</html>-->