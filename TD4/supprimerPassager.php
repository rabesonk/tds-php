<?php
require "Utilisateur.php";

$trajet = Trajet::recupererTrajetParId($_GET['trajet_id']);
if($trajet->supprimerPassager($_GET['login'])) {
    echo "Le passager {$_GET['login']} a été supprimé du trajet {$_GET['trajet_id']} \n";
} else {
    echo "Le trajet {$_GET['trajet_id']} et le passager {$_GET['login']} ne correspondent pas ! \n";
}
?>