<?php
require_once ('../Modele/ModeleUtilisateur.php'); // chargement du modèle
class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require "../vue/$cheminVue"; // Charge la vue
    }
    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        /*require ('../vue/utilisateur/liste.php');*/  //"redirige" vers la vue
        self::afficherVue("../vue/utilisateur/liste.php",["utilisateurs" => $utilisateurs]);
    }
    public static function afficherDetail() : void {
        $user = $_GET['login'];
        $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($user);
        if ($utilisateur == null) {
            /*require ('../vue/utilisateur/erreur.php');*/
            self::afficherVue("../vue/utilisateur/erreur.php");
        }else{
            /*require ('../vue/utilisateur/detail.php');*/
            self::afficherVue("../vue/utilisateur/detail.php",["utilisateur" => $utilisateur]);
        }
    }
    public static function afficherFormulaireCreation ()
    {
        self::afficherVue("../vue/utilisateur/formulaireCreation.php");
    }

    public static function creerDepuisFormulaire(){
        $login = $_GET['login'];
        $nom = $_GET['nom'];
        $prenom = $_GET['prenom'];
        $user = new ModeleUtilisateur($login, $nom, $prenom);
        $user->ajouter();
        self::afficherListe();
    }
}
?>