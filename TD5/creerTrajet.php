<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title> CreerTrajet.php </title>
    </head>

    <body>
    <?php
        require_once ("Trajet.php");
        require_once ("Utilisateur.php");
        $Depart = $_POST['depart'];
        $Arrive = $_POST['arrivee'];
        $Date = $_POST['date'];
        $Prix = $_POST['prix'];
        $Conducteur = $_POST['conducteurLogin'];
        $NonFumeur = isset($_POST["nonFumeur"]);
        $Trajet = new Trajet(null,$Depart,$Arrive,new DateTime($Date),$Prix,Utilisateur::recupererUtilisateurParLogin($Conducteur),$NonFumeur);
        $Trajet->ajouter();
        echo $Trajet;
    ?>
    </body>
</html>