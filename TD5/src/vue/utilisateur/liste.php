<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Liste des utilisateurs</title>
    </head>
    <body>
    <?php
        /** @var ModeleUtilisateur[] $utilisateurs */
        foreach ($utilisateurs as $utilisateur){
            $loginhtml = htmlspecialchars($utilisateur->getLogin());
            $loginURL = rawurlencode($utilisateur->getLogin());
            echo '<p> Utilisateur de login ' . $loginhtml . '.</p>';
            echo "<a href='controleurFrontal.php?action=afficherDetail&login={$loginURL}'>Details</a>";
        }
    echo "<br>";
    echo "<a href='controleurFrontal.php?action=afficherFormulaireCreation'>Créer un utilisateur</a>";
    ?>
    </body>
</html>