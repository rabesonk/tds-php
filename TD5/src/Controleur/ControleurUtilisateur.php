<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\ModeleUtilisateur as ModeleUtilisateur;
//require_once __DIR__ . '/../Modele/ModeleUtilisateur.php'; // chargement du modèle
class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }
    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        /*require ('../vue/utilisateur/liste.php');*/  //"redirige" vers la vue
        self::afficherVue("../vue/vueGenerale.php",["titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php","utilisateurs" => $utilisateurs]);
    }
    public static function afficherDetail() : void {
        $user = $_GET['login'];
        $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($user);
        if ($utilisateur == null) {
            /*require ('../vue/utilisateur/erreur.php');*/
            self::afficherVue("../vue/vueGenerale.php",["titre" => "Erreur avec l'utilisateur","cheminCorpsVue" => "utilisateur/erreur.php"]);
        }else{
            /*require ('../vue/utilisateur/detail.php');*/
            self::afficherVue("../vue/vueGenerale.php",["titre" => "Détails sur l'utilisateur","cheminCorpsVue" => "utilisateur/detail.php","utilisateur" => $utilisateur]);
        }
    }
    public static function afficherFormulaireCreation ()
    {
        self::afficherVue("../vue/vueGenerale.php",["titre" => "Formulaire","cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire(){
        $login = $_GET['login'];
        $nom = $_GET['nom'];
        $prenom = $_GET['prenom'];
        $user = new ModeleUtilisateur($login, $nom, $prenom);
        $user->ajouter();
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs();
        self::afficherVue("../vue/vueGenerale.php",["titre" => "Création d'un utilisateur", "cheminCorpsVue" => "utilisateur/utilisateurCree.php", "utilisateurs" => $utilisateurs]);
    }
}
?>