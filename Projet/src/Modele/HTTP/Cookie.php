<?php
namespace App\Covoiturage\Modele\HTTP;

class Cookie
{
    public static function enregistrer(string $cle, mixed $valeur, ?int $dureeExpiration = null): void {
        $serialiser = serialize($valeur);
        if ($dureeExpiration != null) {
            setcookie($cle, $serialiser, time() + $dureeExpiration);
        }else{
            setcookie($cle, $serialiser);
        }
    }
    public static function lire(string $cle): mixed {
        return unserialize($_COOKIE[$cle]);
    }
    public static function contient($cle) : bool {
        return isset($_COOKIE[$cle]);
    }
    public static function supprimer($cle) : void {
        unset($_COOKIE[$cle]);
        setcookie ($cle, "", 1);
    }
}