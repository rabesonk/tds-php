<?php
namespace App\Covoiturage\Modele\Repository;
use App\Covoiturage\Modele\ConnexionBaseDeDonnees as ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Trajet;
use DateTime;
use App\Covoiturage\Modele\Repository\AbstractRepository;

class TrajetRepository extends AbstractRepository{
    public function construireDepuisTableauSQL(array $trajetTableau) : Trajet {
        $Trajet = new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            new DateTime($trajetTableau["date"]), // À été changer
            $trajetTableau["prix"],
            (new UtilisateurRepository())->recupererParClePrimaire($trajetTableau["conducteurLogin"]), // À été changer
            $trajetTableau["nonFumeur"], // À changer ? Non, voir TD3 Exo 5
        );
        $Trajet->setPassagers(TrajetRepository::recupererPassagers($Trajet));
        return $Trajet;
    }
    /*public static function recupererTrajets() : array {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");

        $trajets = [];
        foreach($pdoStatement as $trajetFormatTableau) {
            $trajets[] = TrajetRepository::construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }*/
    public static function recupererPassagers(Trajet $trajet) : array {
        $Pdo = ConnexionBaseDeDonnees::getPdo();
        $sql = "SELECT passagerLogin FROM passager p
                     JOIN utilisateur u ON  p.passagerLogin = u.login
                     WHERE trajetId = :trajetId";
        $pdoStatement = $Pdo->prepare($sql);
        $values = array(
            "trajetId" => $trajet->getId(),
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);
        $TabPassager = [];
        foreach ($pdoStatement as $UtilisateurFormatTableau) {
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($UtilisateurFormatTableau['passagerLogin']);
            $TabPassager[] = $utilisateur;
        }
        return $TabPassager;
    }
    protected function getNomTable(): string
    {
        return "trajet";
    }
    protected function getNomClePrimaire() : string
    {
        return "id";
    }
    /** @return string[] */
    protected function getNomsColonnes(): array
    {
        return ["id", "depart", "arrivee","date","prix","conducteurLogin","nonFumeur"];
    }

    protected function formatTableauSQL(AbstractDataObject $trajet): array
    {
        return array(
            "id" => $trajet->getId(),
            "depart" => $trajet->getDepart(),
            "arrivee" => $trajet->getArrivee(),
            "date" => $trajet->getDate()->format('Y-m-d'),
            "prix" => $trajet->getPrix(),
            "conducteurLogin" => $trajet->getConducteur()->getLogin(),
            "nonFumeur" => $trajet->getNonFumeur() ?1:0
        );
    }

}