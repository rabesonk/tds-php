<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\ConnexionBaseDeDonnees as ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\TrajetRepository;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;

class UtilisateurRepository extends AbstractRepository {
    public function construireDepuisTableauSQL(array $utilisateurFormatTableau) : Utilisateur
    {
        return new Utilisateur($utilisateurFormatTableau[0],$utilisateurFormatTableau[1],$utilisateurFormatTableau[2],$utilisateurFormatTableau[3],$utilisateurFormatTableau[4],$utilisateurFormatTableau[5],$utilisateurFormatTableau[6],$utilisateurFormatTableau[7] );
    }
    /**
     * @return Trajet[]
     */
    public static function recupererTrajetsCommePassager() : array{
        $sql = "SELECT * FROM trajet
                WHERE id IN (
                    SELECT trajetId FROM passager
                    WHERE passagerLogin = :loginUtilisateur
                );";
        $trajets = [];
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "loginUtilisateur" => Utilisateur::class->getLogin(),
        );
        $pdoStatement->execute($values);
        foreach($pdoStatement as $trajet){
            $trajets[] = (new TrajetRepository())->construireDepuisTableauSQL($trajet);
        }
        return $trajets;
    }

    protected function getNomTable() : string {
        return "p_utilisateur";
    }
    protected function getNomClePrimaire() : string
    {
        return "login";
    }
    /** @return string[] */
    protected function getNomsColonnes(): array
    {
        return ["login", "nom", "prenom", "mdpHache", "estAdmin", "email", "emailAValider", "nonce"];
    }
    protected function formatTableauSQL(AbstractDataObject $utilisateur): array
    {
        return array(
            "login" => $utilisateur->getLogin(),
            "nom" => $utilisateur->getNom(),
            "prenom" => $utilisateur->getPrenom(),
            "mdpHache" => $utilisateur->getMdpHache(),
            "estAdmin" => $utilisateur->getEstAdmin() ? 1 : 0,
            "email" => $utilisateur->getEmail(),
            "emailAValider" => $utilisateur->getEmailAValider(),
            "nonce" => $utilisateur->getNonce(),
        );
    }
}