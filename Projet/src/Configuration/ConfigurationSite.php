<?php
namespace App\Covoiturage\Configuration;

class ConfigurationSite{
    static private int $dureeExpiration = 1800;

    public static function getDureeExpiration() :int {
        return self::$dureeExpiration;
    }

    public static function getUrlAbsolue() : string {
        return "http://localhost/tds-php/Projet/web/controleurFrontal.php";
    }

    public static function getDebug() : bool {
        return false;
    }
}