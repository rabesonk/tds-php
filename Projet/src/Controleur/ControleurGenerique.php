<?php

namespace App\Covoiturage\Controleur;
use App\Covoiturage\Lib\PreferenceControleur;

class ControleurGenerique
{

    protected static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherFormulairePreference (): void {
        self::afficherVue("formulairePreference.php");
    }

    public static function enregistrerPreference() {
        $preference = $_REQUEST["controleur_defaut"];
        PreferenceControleur::enregistrer($preference);
        self::afficherVue("preferenceEnregistree.php");
    }
}