<?php
use App\Covoiturage\Modele\DataObject\Utilisateur;
/**@var Utilisateur $utilisateur*/
$loginHTML = htmlspecialchars($utilisateur->getLogin());
$nom = htmlspecialchars($utilisateur->getNom());
$prenom = htmlspecialchars($utilisateur->getPrenom());
$email = htmlspecialchars($utilisateur->getEmail());
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8" />
    <title> Formulaire mise à jour </title>
</head>

<body>
<form method="<?php echo \App\Covoiturage\Configuration\ConfigurationSite::getDebug() ? 'get' : 'post'?>" action="controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login</label> :
            <input class="InputAddOn-fieldt" type="text" value="<?= $loginHTML?>" name="login" id="login_id" readonly/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">Nom</label> :
            <input class="InputAddOn-field" type="text" placeholder="<?= $nom?>" name="nom" id="nom_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">Prenom</label> :
            <input class="InputAddOn-field" type="text" placeholder="<?= $prenom?>" name="prenom" id="prenom_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp_id">Ancien mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="currentmdp" id="currentmdp_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp_id">Mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp" id="mdp_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp2_id">Vérification du mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp2" id="mdp2_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="email_id">Email&#42;</label>
            <input class="InputAddOn-field" type="email" value="<?= $loginHTML?>" placeholder="toto@yopmail.com" name="email" id="email_id" required>
        </p>
        <?php
        use App\Covoiturage\Lib\ConnexionUtilisateur;
        if(ConnexionUtilisateur::estAdministrateur()){
            echo '<p class="InputAddOn">
            <label class="InputAddOn-item" for="estAdmin_id">Administrateur</label>
            <input class="InputAddOn-field" type="checkbox" placeholder="" name="estAdmin" id="estAdmin_id">
        </p>';
        }
        ?>
        <p>
            <input type="submit" value="Modifier" />
            <input type='hidden' name='action' value='mettreAJour'>
            <input type='hidden' name='controleur' value='utilisateur'>
        </p>
    </fieldset>
</form>
</body>
</html>