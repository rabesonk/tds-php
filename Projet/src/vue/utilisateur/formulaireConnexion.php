<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8" />
    <title> Mon premier php </title>
</head>

<body>
<form method="<?php echo \App\Covoiturage\Configuration\ConfigurationSite::getDebug() ? 'get' : 'post'?>" action="controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login</label> :
            <input class="InputAddOn-fieldt" type="text" value="" placeholder="login" name="login" id="login_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp_id">Mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="currentmdp" id="oldmdp_id" required>
        </p>
        <p>
            <input type="submit" value="Connexion" />
            <input type='hidden' name='action' value='connecter'>
            <input type='hidden' name='controleur' value='utilisateur'>
        </p>
    </fieldset>
</form>
</body>
</html>