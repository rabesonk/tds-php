<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Liste des utilisateurs</title>
</head>
    <body>
    <?php
        use \App\Covoiturage\Lib\ConnexionUtilisateur;
    /** @var Utilisateur $utilisateur */
        $loginhtml = htmlspecialchars($utilisateur->getLogin());
        $nomhtml = htmlspecialchars($utilisateur->getNom());
        $prenomhtml = htmlspecialchars($utilisateur->getPrenom());
        echo '<p> Utilisateur de login ' . $loginhtml . '.</p>';
        echo '<p> Utilisateur de nom: ' . $nomhtml . '.</p>';
        echo '<p> Utilisateur de nom: ' . $prenomhtml . '.</p>';
        if (ConnexionUtilisateur::estUtilisateur($utilisateur->getLogin())) {
            $loginURL = rawurlencode($utilisateur->getLogin());
            echo "<a href='controleurFrontal.php?controleur=utilisateur&action=supprimer&login={$loginURL}'>Supprimer profil</a>";
            echo '    |    ';
            echo "<a href='controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireMiseAJour&login={$loginURL}'>Modifier profil</a>";
            echo '<br>';
            echo '<br>';
        }
    ?>
    </body>
</html>