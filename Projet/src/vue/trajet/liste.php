<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Liste des utilisateurs</title>
    </head>
    <body>
    <?php
        use App\Covoiturage\Modele\DataObject\Trajet;
        /** @var Trajet[] $trajets */
        foreach ($trajets as $trajet){
            $depart = htmlspecialchars($trajet->getDepart());
            $arrivee = htmlspecialchars($trajet->getArrivee());
            $idURL = rawurlencode($trajet->getId());
            echo '<p> Trajet de ' . $depart. ' à '. $arrivee . '.</p>';
            echo "<a href='controleurFrontal.php?controleur=trajet&action=afficherDetail&idTrajet={$idURL}'>Details</a>";
            echo '    |    ';
            echo "<a href='controleurFrontal.php?controleur=trajet&action=supprimer&idTrajet={$idURL}'>Supprimer Trajet</a>";
            echo '    |    ';
            echo "<a href='controleurFrontal.php?controleur=trajet&action=afficherFormulaireMiseAJour&idTrajet={$idURL}'>Modifier Trajet</a>";
            echo '<br>';
            echo '<br>';
        }
        echo "<br>";
        echo "<br>";
        echo "<a href='controleurFrontal.php?controleur=trajet&action=afficherFormulaireCreation'>Créer un trajet</a>";
    ?>
    </body>
</html>