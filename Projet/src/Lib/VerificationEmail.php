<?php
namespace App\Covoiturage\Lib;
use App\Covoiturage\Configuration\ConfigurationSite;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\DataObject\Utilisateur;

class VerificationEmail
{
    public static function envoiEmailValidation(Utilisateur $utilisateur): void
    {
        $destinataire = $utilisateur->getEmailAValider();
        $sujet = "Validation de l'adresse email";
        // Pour envoyer un email contenant du HTML
        $enTete = "MIME-Version: 1.0\r\n";
        $enTete .= "Content-type:text/html;charset=UTF-8\r\n";

        // Corps de l'email
        $loginURL = rawurlencode($utilisateur->getLogin());
        $nonceURL = rawurlencode($utilisateur->getNonce());
        $URLAbsolue = ConfigurationSite::getURLAbsolue();
        $lienValidationEmail = "$URLAbsolue?action=validerEmail&controleur=utilisateur&login=$loginURL&nonce=$nonceURL";
        $corpsEmailHTML = "<a href=\"$lienValidationEmail\">Validation</a>";

        // Temporairement avant d'envoyer un vrai mail
        echo "Simulation d'envoi d'un mail<br> Destinataire : $destinataire<br> Sujet : $sujet<br> Corps : <br>$corpsEmailHTML";

        // Quand vous aurez configué l'envoi de mail via PHP
        // mail($destinataire, $sujet, $corpsEmailHTML, $enTete);
    }

    public static function traiterEmailValidation($login, $nonce): bool
    {
        // À compléter
        $user=(new UtilisateurRepository())->recupererParClePrimaire($login);
        if($user != null){
            if($user->getNonce() == $nonce){
                $email = $user->getEmailAValider();
                $user->setEmail($email);
                $user->setEmailAValider("");
                $user->setNonce("");
                (new UtilisateurRepository())->mettreAJour($user);
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public static function aValideEmail(Utilisateur $utilisateur) : bool
    {
        // À compléter
        if($utilisateur->getEmailAValider() == ""){
            return true;
        }else{
            return false;
        }
    }
}