<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\DataObject\Utilisateur;
//require_once __DIR__ . '/../Modele/Utilisateur.php'; // chargement du modèle
class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }
    public static function afficherErreur(string $messageErreur = ""){
        self::afficherVue("../vue/vueGenerale.php",["titre" => "Erreur avec l'utilisateur","cheminCorpsVue" => "utilisateur/erreur.php", "messageErreur" => $messageErreur]);
    }
    public static function afficherListe() : void {
        $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        /*require ('../vue/utilisateur/liste.php');*/  //"redirige" vers la vue
        self::afficherVue("../vue/vueGenerale.php",["titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php","utilisateurs" => $utilisateurs]);
    }
    public static function afficherDetail() : void {
        if(isset($_GET["login"])){
            $user = $_GET['login'];
        }else{
            $user = "";
        }
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($user);
        if ($utilisateur == null) {
            /*require ('../vue/utilisateur/erreur.php');*/
            self::afficherErreur("L'utilisateur n'existe pas");
        }else{
            /*require ('../vue/utilisateur/detail.php');*/
            self::afficherVue("../vue/vueGenerale.php",["titre" => "Détails sur l'utilisateur","cheminCorpsVue" => "utilisateur/detail.php","utilisateur" => $utilisateur]);
        }
    }
    public static function afficherFormulaireCreation ()
    {
        self::afficherVue("../vue/vueGenerale.php",["titre" => "Formulaire","cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire(){
        $user = self::construireDepuisFormulaire($_GET);
        (new UtilisateurRepository())->ajouter($user);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue("../vue/vueGenerale.php",["titre" => "Création d'un utilisateur", "cheminCorpsVue" => "utilisateur/utilisateurCree.php", "utilisateurs" => $utilisateurs]);
    }

    public static function supprimer(){
        $login = htmlspecialchars($_GET['login']);
        (new UtilisateurRepository)->supprimer($login);
        self::afficherVue("../vue/vueGenerale.php",["titre" => "Supression","cheminCorpsVue" => "utilisateur/utilisateurSupprime.php","login" => $login,"utilisateurs" => (new UtilisateurRepository())->recuperer()]);
    }

    public static function afficherFormulaireMiseAJour(){
        $login = htmlspecialchars($_GET['login']);
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        if ($utilisateur == null) {
            self::afficherErreur("L'utilisateur n'existe pas");
        }else{
            self::afficherVue("../vue/vueGenerale.php",["titre" => "Modifier l'utilisateur","cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php","utilisateur" => $utilisateur]);
        }
    }

    public static function mettreAJour(){
        $user = self::construireDepuisFormulaire($_GET);
        (new UtilisateurRepository())->mettreAJour($user);
        self::afficherVue("../vue/vueGenerale.php",["titre" => "Utilisateur a été modifié","cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php","login" => htmlspecialchars($user->getLogin()), "utilisateurs" => (new UtilisateurRepository())->recuperer()]);
    }

    /**
     * @return Utilisateur
     */
    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        $login = $tableauDonneesFormulaire['login'];
        $nom = $tableauDonneesFormulaire['nom'];
        $prenom = $tableauDonneesFormulaire['prenom'];
        return new Utilisateur($login, $nom, $prenom);
    }
}
?>