<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Liste des utilisateurs</title>
</head>
<body>
    <?php
    use App\Covoiturage\Modele\DataObject\AbstractDataObject;
    /** @var AbstractDataObject $trajet */
    $nonFumeur = $trajet->getNonFumeur() ? " non fumeur" : " ";
    $depart = htmlspecialchars($trajet->getDepart());
    $arrivee = htmlspecialchars($trajet->getArrivee());
    $prenomConducteur = htmlspecialchars($trajet->getConducteur()->getPrenom());
    $nomConducteur = htmlspecialchars($trajet->getConducteur()->getNom());
    echo "<p>
                Le trajet {$nonFumeur} du {$trajet->getDate()->format("d/m/Y")} partira de {$depart} pour aller à {$arrivee} (conducteur: {$prenomConducteur} {$nomConducteur}).
            </p>";
    ?>
</body>
</html>