<?php
require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';
//require_once __DIR__ . '/../src/Controleur/ControleurUtilisateur.php';
use App\Covoiturage\Controleur\ControleurUtilisateur as ControleurUtilisateur;

// initialisation en activant l'affichage de débogage
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');

$nomDeClasseControleur = "App\Covoiturage\Controleur\Controleur";
if(isset($_GET['controleur'])){
    $controleur = $_GET['controleur'];
    $nomDeClasseControleur = $nomDeClasseControleur.ucfirst($controleur);
}else{
    $controleur = "utilisateur";
    $nomDeClasseControleur = $nomDeClasseControleur.ucfirst($controleur);
}
if(class_exists($nomDeClasseControleur)){
    // On récupère l'action passée dans l'URL
    if(isset($_GET['action'])){
        if(in_array($_GET['action'],get_class_methods($nomDeClasseControleur))){
            $action = $_GET['action'];
            // Appel de la méthode statique $action de $nomDeClasseControleur
            $nomDeClasseControleur::$action();
        }else{
            $nomDeClasseControleur::afficherErreur("l'action $_GET[action] n'existe pas");
        }
    }else{
        $action = 'afficherListe';
        // Appel de la méthode statique $action de $nomDeClasseControleur
        $nomDeClasseControleur::$action();
    }
}else{
    $nomDeClasseControleur::afficherErreur("Ce controleur n'existe pas");
}

?>