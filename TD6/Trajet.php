<?php

require_once __DIR__ . '/src/Modele/ConnexionBaseDeDonnees.php';
require_once __DIR__ . '/src/Modele/Utilisateur.php';

class Trajet {

    private ?int $id;
    private string $depart;
    private string $arrivee;
    private DateTime $date;
    private int $prix;
    private Utilisateur $conducteur;
    private bool $nonFumeur;
    /**
     * @var Utilisateur[]
     */
    private array $passagers;

    public function __construct(
        ?int $id,
        string $depart,
        string $arrivee,
        DateTime $date,
        int $prix,
        Utilisateur $conducteur,
        bool $nonFumeur
    )
    {
        $this->id = $id;
        $this->depart = $depart;
        $this->arrivee = $arrivee;
        $this->date = $date;
        $this->prix = $prix;
        $this->conducteur = $conducteur;
        $this->nonFumeur = $nonFumeur;
        $this->passagers = [];
    }

    public static function construireDepuisTableauSQL(array $trajetTableau) : Trajet {
         $Trajet = new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            new DateTime($trajetTableau["date"]), // À été changer
            $trajetTableau["prix"],
            Utilisateur::recupererUtilisateurParLogin($trajetTableau["conducteurLogin"]), // À été changer
            $trajetTableau["nonFumeur"], // À changer ? Non, voir TD3 Exo 5
        );
         $Trajet->setPassagers($Trajet->recupererPassagers());
         return $Trajet;
    }

    public function getPassagers(): array
    {
        return $this->passagers;
    }

    public function setPassagers(array $passagers): void
    {
        $this->passagers = $passagers;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getDepart(): string
    {
        return $this->depart;
    }

    public function setDepart(string $depart): void
    {
        $this->depart = $depart;
    }

    public function getArrivee(): string
    {
        return $this->arrivee;
    }

    public function setArrivee(string $arrivee): void
    {
        $this->arrivee = $arrivee;
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function setDate(DateTime $date): void
    {
        $this->date = $date;
    }

    public function getPrix(): int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): void
    {
        $this->prix = $prix;
    }

    public function getConducteur(): Utilisateur
    {
        return $this->conducteur;
    }

    public function setConducteur(Utilisateur $conducteur): void
    {
        $this->conducteur = $conducteur;
    }

    public function isNonFumeur(): bool
    {
        return $this->nonFumeur;
    }

    public function setNonFumeur(bool $nonFumeur): void
    {
        $this->nonFumeur = $nonFumeur;
    }

    public function __toString()
    {
        $nonFumeur = $this->nonFumeur ? " non fumeur" : " ";
        return "<p>
            Le trajet$nonFumeur du {$this->date->format("d/m/Y")} partira de {$this->depart} pour aller à {$this->arrivee} (conducteur: {$this->conducteur->getPrenom()} {$this->conducteur->getNom()}).
        </p>";
    }

    /**
     * @return Trajet[]
     */
    public static function recupererTrajets() : array {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");

        $trajets = [];
        foreach($pdoStatement as $trajetFormatTableau) {
            $trajets[] = Trajet::construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }

    public function ajouter() : void {
        $sql = "INSERT INTO trajet (depart,arrivee,date,prix,conducteurLogin,nonFumeur) VALUES (:depart,:arrivee,:date,:prix,:conducteurLogin,:nonFumeur)";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "depart" => $this->depart,
            "arrivee" => $this->arrivee,
            "date" => $this->date->format("Y-m-d"),
            "prix" => $this->prix,
            "conducteurLogin" => $this->conducteur->getLogin(),
            "nonFumeur" => $this->nonFumeur ?1:0
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);
    }
    /**
     * @return Utilisateur[]
     */
    private function recupererPassagers() : array {
        $Pdo = ConnexionBaseDeDonnees::getPdo();
        $sql = "SELECT passagerLogin FROM passager p
                     JOIN utilisateur u ON  p.passagerLogin = u.loginBaseDeDonnees
                     WHERE trajetId = :trajetId";
        $pdoStatement = $Pdo->prepare($sql);
        $values = array(
            "trajetId" => $this->id,
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);
        $TabPassager = [];
        foreach ($pdoStatement as $UtilisateurFormatTableau) {
            $utilisateur = Utilisateur::recupererUtilisateurParLogin($UtilisateurFormatTableau['passagerLogin']);
            $TabPassager[] = $utilisateur;
        }
        return $TabPassager;
    }

    public function supprimerPassager(string $passagerLogin): bool {
        return true;
    }

    public static function recupererTrajetParId(string $trajetId) : ?Trajet {
        $sql = "SELECT * from trajet WHERE id = :idTrajet";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "idTrajet" => $trajetId,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);
        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $utilisateurFormatTableau = $pdoStatement->fetch();
        if (empty($utilisateurFormatTableau)){
            return null;
        }else{
            return Trajet::construireDepuisTableauSQL($utilisateurFormatTableau);
        }
    }
}
