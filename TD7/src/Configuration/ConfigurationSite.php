<?php
namespace App\Covoiturage\Configuration;

class ConfigurationSite{
    static private int $dureeExpiration = 1800;

    public static function getDureeExpiration() :int {
        return self::$dureeExpiration;
    }
}