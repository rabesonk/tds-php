<?php
use \App\Covoiturage\Modele\DataObject\AbstractDataObject;
/**@var AbstractDataObject $trajet*/
$depart = htmlspecialchars($trajet->getDepart());
$arrivee = htmlspecialchars($trajet->getArrivee());
$date = $trajet->getDate()->format('Y-m-d');
$prix = $trajet->getPrix();
$conducteur = htmlspecialchars($trajet->getConducteur()->getLogin());
$nonfumeur = $trajet->getNonfumeur();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8" />
    <title> Creation Trajet </title>
</head>
<body>
<div>
    <form method="get" action="controleurFrontal.php">
        <!-- Remplacer method="get" par method="post" pour changer le format d'envoi des données -->
        <fieldset>
            <legend>Mon formulaire :</legend>
            <p>
                <label for="depart_id">Depart</label> :
                <input type="text" value="<?= $depart?>" name="depart" id="depart_id" required/>
            </p>
            <p>
                <label for="arrivee_id">Arrivée</label> :
                <input type="text" value="<?= $arrivee?>" name="arrivee" id="arrivee_id" required/>
            </p>
            <p>
                <label for="date_id">Date</label> :
                <input type="date" value="<?= $date?>" name="date" id="date_id"  required/>
            </p>
            <p>
                <label for="prix_id">Prix</label> :
                <input type="number" value="<?= $prix?>" name="prix" id="prix_id"  required/>
            </p>
            <p>
                <label for="conducteurLogin_id">Login du conducteur</label> :
                <input type="text" value="<?= $conducteur?>" name="conducteurLogin" id="conducteurLogin_id" required/>
            </p>
            <p>
                <label for="nonFumeur_id">Non Fumeur ?</label> :
                <input type="checkbox" <?php echo $nonfumeur ?'checked':''; ?> name="nonFumeur" id="nonFumeur_id"/>
            </p>
            <p>
                <input type="submit" value="Modifier" />
                <input type='hidden' name='action' value='mettreAjour'>
                <input type='hidden' name='controleur' value='trajet'>
                <input type='hidden' name='id' value='<?=$trajet->getId()?>'>
        </fieldset>
    </form>
</div>
</body>
</html>