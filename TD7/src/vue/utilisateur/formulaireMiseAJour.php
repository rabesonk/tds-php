<?php
use App\Covoiturage\Modele\DataObject\Utilisateur;
/**@var Utilisateur $utilisateur*/
$loginHTML = htmlspecialchars($utilisateur->getLogin());
$nom = htmlspecialchars($utilisateur->getNom());
$prenom = htmlspecialchars($utilisateur->getPrenom());
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8" />
    <title> Mon premier php </title>
</head>

<body>
<form method="get" action="controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login</label> :
            <input class="InputAddOn-fieldt" type="text" value="<?= $loginHTML?>" name="login" id="login_id" readonly/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">Nom</label> :
            <input class="InputAddOn-field" type="text" placeholder="<?= $nom?>" name="nom" id="nom_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">Prenom</label> :
            <input class="InputAddOn-field" type="text" placeholder="<?= $prenom?>" name="prenom" id="prenom_id" required/>
        </p>
        <p>
            <input type="submit" value="Modifier" />
            <input type='hidden' name='action' value='mettreAJour'>
            <input type='hidden' name='controleur' value='utilisateur'>
        </p>
    </fieldset>
</form>
</body>
</html>