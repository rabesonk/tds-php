<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\Repository\TrajetRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use DateTime;

//require_once __DIR__ . '/../Modele/Utilisateur.php'; // chargement du modèle
class ControleurTrajet extends ControleurGenerique {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    protected static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }
    public static function afficherErreur(string $messageErreur = ""){
        self::afficherVue("../vue/vueGenerale.php",["titre" => "Erreur avec le trajet","cheminCorpsVue" => "trajet/erreur.php", "messageErreur" => $messageErreur]);
    }
    public static function afficherListe() : void {
        $trajets = (new TrajetRepository())->recuperer(); //appel au modèle pour gérer la BD
        /*require ('../vue/utilisateur/liste.php');*/  //"redirige" vers la vue
        self::afficherVue("../vue/vueGenerale.php",["titre" => "Liste des trajets", "cheminCorpsVue" => "trajet/liste.php","trajets" => $trajets]);
    }

    public static function afficherDetail() : void {
        if(isset($_GET["idTrajet"])){
            $IdTratjet = $_GET['idTrajet'];
        }else{
            $IdTratjet = "";
        }
        $trajet = (new TrajetRepository())->recupererParClePrimaire($IdTratjet);
        if ($trajet == null) {
            self::afficherErreur("Ce trajet n'existe pas");
        }else{
            self::afficherVue("../vue/vueGenerale.php",["titre" => "Détails sur le trajet","cheminCorpsVue" => "trajet/detail.php","trajet" => $trajet]);
        }
    }

    public static function afficherFormulaireCreation ()
    {
        self::afficherVue("../vue/vueGenerale.php",["titre" => "Formulaire","cheminCorpsVue" => "trajet/formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire(){
        $trajet = self::construireDepuisFormulaire($_GET);
        (new TrajetRepository())->ajouter($trajet);
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue("../vue/vueGenerale.php",["titre" => "Création d'un trajet", "cheminCorpsVue" => "trajet/trajetCree.php", "trajets" => $trajets]);
    }

    public static function supprimer(){
        $IDtrajet = htmlspecialchars($_GET['idTrajet']);
        (new TrajetRepository())->supprimer($IDtrajet);
        self::afficherVue("../vue/vueGenerale.php",["titre" => "Supression","cheminCorpsVue" => "trajet/trajetSupprime.php","trajet" => $IDtrajet,"trajets" => (new TrajetRepository())->recuperer()]);
    }

    public static function afficherFormulaireMiseAJour(){
        $id = htmlspecialchars($_GET['idTrajet']);
        $trajet = (new TrajetRepository())->recupererParClePrimaire($id);
        if ($trajet == null) {
            self::afficherErreur("Le trajet n'existe pas");
        }else{
            self::afficherVue("../vue/vueGenerale.php",["titre" => "Modifier le trajet","cheminCorpsVue" => "trajet/formulaireMiseAJour.php","trajet" => $trajet]);
        }
    }
    
    public static function mettreAjour(){
        $trajet =  self::construireDepuisFormulaire($_GET);
        (new TrajetRepository())->mettreAJour($trajet);
        self::afficherVue("../vue/vueGenerale.php",["titre" => "Trajet a été modifié","cheminCorpsVue" => "trajet/trajetMisAJour.php","idTrajet" => $trajet->getId(), "trajets" => (new TrajetRepository())->recuperer()]);
    }
    
    /**
     * @return Trajet
     * @throws \DateMalformedStringException
     */
    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Trajet
    {
        $id = $tableauDonneesFormulaire["id"] ?? null;
        $Depart = $tableauDonneesFormulaire['depart'];
        $Arrive = $tableauDonneesFormulaire['arrivee'];
        $Date = $tableauDonneesFormulaire['date'];
        $Prix = $tableauDonneesFormulaire['prix'];
        $Conducteur = $tableauDonneesFormulaire['conducteurLogin'];
        $NonFumeur = isset($tableauDonneesFormulaire["nonFumeur"]);
        return new Trajet($id, $Depart, $Arrive, new DateTime($Date), $Prix, (new UtilisateurRepository())->recupererParClePrimaire($Conducteur), $NonFumeur);
    }
}
?>