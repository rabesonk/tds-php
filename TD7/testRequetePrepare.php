<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title> Utilisateur.php </title>
    </head>
    <body>
        <h3>Voici le résultat</h3>
        <?php
            require_once 'Utilisateur.php';
            $utilisateur = Utilisateur::recupererUtilisateurParLogin("leblancj");
            echo $utilisateur;
            echo '<br>';
            $utilisateur2 = new Utilisateur("arquea","Arque","Aurelien");
            /*Deja dans la BDD suite au 1er test, ne pas réexecuter la ligne suivante si les valeurs arquea,etc n'ont pas été changer*/
            /*$utilisateur2->ajouter();*/
            $test = Utilisateur::recupererUtilisateurParLogin("arquea");
            echo $test;
        ?>
    </body>
</html>