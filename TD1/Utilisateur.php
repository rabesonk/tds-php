<?php
class Utilisateur {

    private string $login;
    private string $nom;
    private string $prenom;

    // un getter
    public function getNom() : string {
        return $this->nom;
    }

    // un setter
    public function setNom($nom) : void {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getPrenom() : string
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom($prenom): void
    {
        $this->prenom = $prenom;
    }

    /**
     * @return mixed
     */
    public function getLogin() : string
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin($login): void
    {
        if (strlen($login) > 64) {
            $this->login = substr($login, 0, 64);
        }else{
            $this->login = $login;
        }
    }

    // un constructeur
    public function __construct(
      string $login,
      string $nom,
      string $prenom,
   ) {
        if (strlen($login) > 64) {
            $this->login = substr($login, 0, 65);
        }else{
            $this->login = $login;
        }
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
    public function __toString() : string {
      // À compléter dans le prochain exercicere
        return "Utilisateur {$this->prenom} {$this->nom} de login {$this->login}";
    }
}
/*$utilisateur1 = new Utilisateur("rabesonk","Rabeson","Khylian");
$utilisateur2 = new Utilisateur("Leschatssontdesanimauxdomestiquestrèspopulairesdanslemondeentieraujourd'hui","longueurLogin","Test");*/
?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title> Utilisateur.php </title>
    </head>

    <body>
        <h3>Voici le résultat</h3>
        <!--<p><?php
/*            echo $utilisateur1;
        */?></p>
        <p><?php
/*            echo "la phrase de longeur > 64 : Leschatssontdesanimauxdomestiquestrèspopulairesdanslemondeentieraujourd'hui";
            echo "<br>";
            echo $utilisateur2;
        */?></p>-->
    </body>
</html>