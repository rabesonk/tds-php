<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title> testUtilisateur.php </title>
    </head>

    <body>
        <?php
        require_once ("Utilisateur.php");
        $utilisateur1 = new Utilisateur("judabricot","bricot","juda");
        echo $utilisateur1;
        ?>
        <p><?php
            echo " modification va créer une erreur";
            echo "<br>";
            $utilisateur1->setNom(["test"]);
            echo $utilisateur1;
        ?></p>
    </body>
</html>
