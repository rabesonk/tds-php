<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>

    <body>
        Voici le résultat du script PHP :
        <?php
          // Ceci est un commentaire PHP sur une ligne
          /* Ceci est le 2ème type de commentaire PHP
          sur plusieurs lignes */

          // On met la chaine de caractères "hello" dans la variable 'texte'
          // Les noms de variable commencent par $ en PHP
          $texte = "hello world !";

          // On écrit le contenu de la variable 'texte' dans la page Web
          echo $texte;
        $prenom = "Marc";

        echo "Bonjour\n " . $prenom;
        echo "Bonjour\n $prenom";
        echo 'Bonjour\n $prenom';

        echo $prenom;
        echo "$prenom";
        ?>
        <p>
            <?php
            $prenom = "Juste";
            $nom = "LeBlanc";
            $login = "leblancj";
            echo "Utilisateur  $prenom $nom de $login";
            ?>
        </p>
        <br>
        <?php
        $utilisateur = [
            'prenom' => 'Juste',
            'nom'    => 'Leblanc',
            'login' => 'leblancj'
        ];
        var_dump($utilisateur);?>
        <br>
        <?php
        echo "Utilisateur {$utilisateur["prenom"]} {$utilisateur["nom"]} de {$utilisateur["login"]}";
        ?>
        <br>
        <?php
        $utilisateur2 = [
            [
                'prenom' => 'Juste',
                'nom'    => 'Leblanc',
                'login' => 'leblancj'
            ],
            [
                'prenom' => 'Khylian',
                'nom'    => 'Rabeson',
                'login' => 'rabesonk'
            ],
            [
                'prenom' => 'Goharik',
                'nom'    => 'Gyurjyan',
                'login' => 'gyurjyang'
            ]
        ];
        var_dump($utilisateur2);
        ?>
        <br>
        <h3>Liste des utilisateurs :</h3>
        <ul>
            <?php
                foreach ($utilisateur2 as $item) {
                echo "<li>";
                foreach ($item as $key => $value) {
                    echo $value;
                    echo "<br>";
                }
                echo "</li>";
            }
            ?>
        </ul>
        <p>Exo 8, finir le reste</p>
    </body>
</html>