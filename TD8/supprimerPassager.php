<?php
require "Utilisateur.php";

$trajet = Trajet::recupererTrajetParId($_REQUEST['trajet_id']);
$login = rawurldecode($_REQUEST['login']);
if($trajet->supprimerPassager($login)) {
    echo "Le passager {$login} a été supprimé du trajet {$_REQUEST['trajet_id']} \n";
} else {
    echo "Le trajet {$_REQUEST['trajet_id']} et le passager {$login} ne correspondent pas ! \n";
}
?>