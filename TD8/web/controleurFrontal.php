<?php
require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';
//require_once __DIR__ . '/../src/Controleur/ControleurUtilisateur.php';
use App\Covoiturage\Controleur\ControleurUtilisateur as ControleurUtilisateur;
use App\Covoiturage\Lib\PreferenceControleur;

// initialisation en activant l'affichage de débogage
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');

$nomDeClasseControleur = "App\Covoiturage\Controleur\Controleur";
if(isset($_REQUEST['controleur'])){
    $controleur = $_REQUEST['controleur'];
    $nomDeClasseControleur = $nomDeClasseControleur.ucfirst($controleur);
}else{
    if (isset($_COOKIE['preferenceControleur'])) {
        $controleur = PreferenceControleur::lire();
    }else{
        $controleur = "utilisateur";
    }
    $nomDeClasseControleur = $nomDeClasseControleur.ucfirst($controleur);
}
if(class_exists($nomDeClasseControleur)){
    // On récupère l'action passée dans l'URL
    if(isset($_REQUEST['action'])){
        if(in_array($_REQUEST['action'],get_class_methods($nomDeClasseControleur))){
            $action = $_REQUEST['action'];
            // Appel de la méthode statique $action de $nomDeClasseControleur
            $nomDeClasseControleur::$action();
        }else{
            $nomDeClasseControleur::afficherErreur("l'action $_REQUEST[action] n'existe pas");
        }
    }else{
        $action = 'afficherListe';
        // Appel de la méthode statique $action de $nomDeClasseControleur
        $nomDeClasseControleur::$action();
    }
}else{
    $nomDeClasseControleur::afficherErreur("Ce controleur n'existe pas");
}

?>