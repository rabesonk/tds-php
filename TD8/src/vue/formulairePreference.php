<?php
namespace App\Covoiturage\vue;
use App\Covoiturage\Lib\PreferenceControleur;
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8" />
    <title> Creation Trajet </title>
</head>
<body>
<div>
    <form method="<?php echo \App\Covoiturage\Configuration\ConfigurationSite::getDebug() ? 'get' : 'post'?>" action="controleurFrontal.php">
        <!-- Remplacer method="get" par method="post" pour changer le format d'envoi des données -->
        <fieldset>
            <legend>Mon formulaire :</legend>
            <input type="radio" id="utilisateurId" name="controleur_defaut" value="utilisateur" <?php echo PreferenceControleur::lire()=='utilisateur'?'checked':''; ?>>
            <label for="utilisateurId">Utilisateur</label>
            <input type="radio" id="trajetId" name="controleur_defaut" value="trajet" <?php  echo PreferenceControleur::lire()=='trajet'?'checked':''; ?>>
            <label for="trajetId">Trajet</label>
            <input type="submit" value="Envoyer" />
            <input type='hidden' name='action' value='enregistrerPreference'>
        </fieldset>
    </form>
</div>
</body>
</html>