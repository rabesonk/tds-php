<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Liste des utilisateurs</title>
    </head>
    <body>
    <?php
        /** @var ModeleUtilisateur[] $utilisateurs */

    use App\Covoiturage\Lib\ConnexionUtilisateur;

    foreach ($utilisateurs as $utilisateur){
            $loginhtml = htmlspecialchars($utilisateur->getLogin());
            $loginURL = rawurlencode($utilisateur->getLogin());
            echo '<p> Utilisateur de login ' . $loginhtml . '.</p>';
            echo "<a href='controleurFrontal.php?controleur=utilisateur&action=afficherDetail&login={$loginURL}'>Details</a>";
            if (ConnexionUtilisateur::estAdministrateur()) {
                echo '    |    ';
                echo "<a href='controleurFrontal.php?controleur=utilisateur&action=supprimer&login={$loginURL}'>Supprimer profil</a>";
                echo '    |    ';
                echo "<a href='controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireMiseAJour&login={$loginURL}'>Modifier profil</a>";
                echo '<br>';
                echo '<br>';
            }
        }
        echo "<br>";
        echo "<br>";
        echo "<a href='controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireCreation'>Créer un utilisateur</a>";
    ?>
    </body>
</html>