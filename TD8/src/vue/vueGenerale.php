<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../../TD8/ressources/css/style.css">
        <title><?php
            /**
             * @var string $titre
             */
            echo $titre; ?></title>
    </head>
    <body>
        <header>
            <nav>
                <ul>
                    <li>
                        <a href="controleurFrontal.php?action=afficherListe&controleur=utilisateur">Gestion des utilisateurs</a>
                    </li><li>
                        <a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Gestion des trajets</a>
                    </li>
                    <li>
                        <a href="controleurFrontal.php?action=afficherFormulairePreference">
                            <img src="../../ressources/img/heart.png">
                        </a>
                    </li>
                    <?php
                        use \App\Covoiturage\Lib\ConnexionUtilisateur;
                        if (ConnexionUtilisateur::estConnecte() == false) {
                            echo '
                                <li>
                                    <a href="controleurFrontal.php?action=afficherFormulaireCreation">
                                        <img src="../../ressources/img/add-user.png">
                                    </a>
                                </li>
                                <li>
                                    <a href="controleurFrontal.php?action=afficherFormulaireConnexion">
                                        <img src="../../ressources/img/enter.png">
                                    </a>
                                </li>
                            ';
                        } else {
                            $login = rawurlencode(ConnexionUtilisateur::getLoginUtilisateurConnecte());
                            echo "
                                <li>
                                    <a href='controleurFrontal.php?action=afficherDetail&login={$login}'>
                                        <img src='/TD8/ressources/img/user.png'>
                                    </a>
                                </li>
                                <li>
                                    <a href='controleurFrontal.php?action=deconnecter'>
                                        <img src='/TD8/ressources/img/logout.png'>
                                    </a>
                                </li>
                            ";
                        }
                    ?>
                </ul>
            </nav>
        </header>
        <main>
            <?php
            /**
             * @var string $cheminCorpsVue
             */
            require __DIR__ . "/{$cheminCorpsVue}";
            ?>
        </main>
        <footer>
            <p>
                Site de covoiturage de Khylian RABESON
            </p>
        </footer>
    </body>
</html>