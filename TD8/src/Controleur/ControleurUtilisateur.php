<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Lib\MotDePasse;
use App\Covoiturage\Lib\VerificationEmail;
use App\Covoiturage\Modele\HTTP\Cookie;
use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\DataObject\Utilisateur;
//require_once __DIR__ . '/../Modele/Utilisateur.php'; // chargement du modèle
class ControleurUtilisateur extends ControleurGenerique {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherErreur(string $messageErreur = ""){
        self::afficherVue("../vue/vueGenerale.php",["titre" => "Erreur avec l'utilisateur","cheminCorpsVue" => "utilisateur/erreur.php", "messageErreur" => $messageErreur]);
    }
    public static function afficherListe() : void {
        $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        /*require ('../vue/utilisateur/liste.php');*/  //"redirige" vers la vue
        self::afficherVue("../vue/vueGenerale.php",["titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php","utilisateurs" => $utilisateurs]);
    }
    public static function afficherDetail() : void {
        if(isset($_REQUEST["login"])){
            $user = rawurldecode($_REQUEST['login']);
        }else{
            $user = "";
        }
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($user);
        if ($utilisateur == null) {
            /*require ('../vue/utilisateur/erreur.php');*/
            self::afficherErreur("L'utilisateur n'existe pas");
        }else{
            /*require ('../vue/utilisateur/detail.php');*/
            self::afficherVue("../vue/vueGenerale.php",["titre" => "Détails sur l'utilisateur","cheminCorpsVue" => "utilisateur/detail.php","utilisateur" => $utilisateur]);
        }
    }
    public static function afficherFormulaireCreation ()
    {
        self::afficherVue("../vue/vueGenerale.php",["titre" => "Formulaire","cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire(){
        if(ConnexionUtilisateur::estAdministrateur()){
            if($_REQUEST['mdp'] != $_REQUEST['mdp2']){
                self::afficherErreur("Les mots de passes ne correspondent pas");
            }elseif (filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL) == false){
                self::afficherErreur("L'email n'est pas correcte");
            } else{
                $user = self::construireDepuisFormulaire($_REQUEST);
                (new UtilisateurRepository())->ajouter($user);
                VerificationEmail::envoiEmailValidation($user);
                $utilisateurs = (new UtilisateurRepository())->recuperer();
                self::afficherVue("../vue/vueGenerale.php",["titre" => "Création d'un utilisateur", "cheminCorpsVue" => "utilisateur/utilisateurCree.php", "utilisateurs" => $utilisateurs]);
            }
        }else{
            if($_REQUEST['mdp'] != $_REQUEST['mdp2']){
                self::afficherErreur("Les mots de passes ne correspondent pas");
            }elseif (filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL) == false){
                self::afficherErreur("L'email n'est pas correcte");
            }else{
                $user = self::construireDepuisFormulaire($_REQUEST);
                (new UtilisateurRepository())->ajouter($user);
                VerificationEmail::envoiEmailValidation($user);
                $utilisateurs = (new UtilisateurRepository())->recuperer();
                self::afficherVue("../vue/vueGenerale.php",["titre" => "Création d'un utilisateur", "cheminCorpsVue" => "utilisateur/utilisateurCree.php", "utilisateurs" => $utilisateurs]);
            }
        }
    }

    public static function supprimer(){
        if(ConnexionUtilisateur::estUtilisateur(rawurldecode($_REQUEST['login']))){
            $login = htmlspecialchars(rawurldecode($_REQUEST['login']));
            (new UtilisateurRepository)->supprimer($login);
            ConnexionUtilisateur::deconnecter();
            self::afficherVue("../vue/vueGenerale.php",["titre" => "Supression","cheminCorpsVue" => "utilisateur/utilisateurSupprime.php","login" => $login,"utilisateurs" => (new UtilisateurRepository())->recuperer()]);
        }else{
            self::afficherErreur("La mise à jour n’est possible que pour l’utilisateur connecté.");
        }
    }

    public static function afficherFormulaireMiseAJour(){
        $logindecode = rawurldecode($_REQUEST['login']);
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($logindecode);
        if(ConnexionUtilisateur::estUtilisateur($logindecode) || ConnexionUtilisateur::estAdministrateur()){
            if ($utilisateur == null) {
                self::afficherErreur("L'utilisateur n'existe pas");
            }else{
                self::afficherVue("../vue/vueGenerale.php",["titre" => "Modifier l'utilisateur","cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php","utilisateur" => $utilisateur]);
            }
        }else{
            self::afficherErreur("La mise à jour n’est possible que pour l’utilisateur connecté.");
        }
    }

    public static function mettreAJour(){
        if (ConnexionUtilisateur::estAdministrateur()){
            if ($_REQUEST['mdp'] != $_REQUEST['mdp2']){
                self::afficherErreur("Les mots de passes ne correspondent pas");
            }else{
                $User = (new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login']);
                $User->setlogin($_REQUEST['login']);
                $User->setNom($_REQUEST['nom']);
                $User->setPrenom($_REQUEST['prenom']);
                $User->setMdpHache(MotDePasse::hacher($_REQUEST['mdp']));
                $User->setEstAdmin(isset($_REQUEST['estAdmin']));
                if($User->getEmail() != $_REQUEST['email']){
                    if (filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL) == false){
                        self::afficherErreur("L'email n'est pas correcte");
                    }else{
                        $User->setEmail("");
                        $User->setEmailAValider($_REQUEST['email']);
                        $User->setNonce(MotDePasse::genererChaineAleatoire());
                        VerificationEmail::envoiEmailValidation($User);
                    }
                }
                (new UtilisateurRepository())->mettreAJour($User);
                self::afficherVue("../vue/vueGenerale.php",["titre" => "Utilisateur a été modifié","cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php","login" => $User->getLogin(), "utilisateurs" => (new UtilisateurRepository())->recuperer()]);
            }
        }else{
            if(ConnexionUtilisateur::estUtilisateur($_REQUEST['login'])){
                $User = (new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login']);
                if(!MotDePasse::verifier($_REQUEST['currentmdp'],$User->getMdpHache())){
                    self::afficherErreur("Ce n'est pas votre mot de passe actuel");
                } else if ($_REQUEST['mdp'] != $_REQUEST['mdp2']){
                    self::afficherErreur("Les mots de passes ne correspondent pas");
                }else{
                    /*$newUser = self::construireDepuisFormulaire($_REQUEST);*/
                    $User->setlogin($_REQUEST['login']);
                    $User->setNom($_REQUEST['nom']);
                    $User->setPrenom($_REQUEST['prenom']);
                    $User->setMdpHache(MotDePasse::hacher($_REQUEST['mdp']));
                    (new UtilisateurRepository())->mettreAJour($User);
                    self::afficherVue("../vue/vueGenerale.php",["titre" => "Utilisateur a été modifié","cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php","login" => $newUser->getLogin(), "utilisateurs" => (new UtilisateurRepository())->recuperer()]);
                }
            } else {
                self::afficherErreur("Vous devez être connecté pour modifier l'utilisateur.");
            }
        }
    }

    public static function afficherFormulaireConnexion(){
        self::afficherVue("../vue/vueGenerale.php",["titre" => "Connexion utilisateur","cheminCorpsVue" => "utilisateur/formulaireConnexion.php"]);
    }

    public static function connecter(){
        if(!isset($_REQUEST["login"])){
            if(!isset($_REQUEST["currentmdp"])){
                self::afficherErreur("Login et mot de passe manquants");
            } else {
                self::afficherErreur("Login manquant");
            }
        }
        $user = (new UtilisateurRepository())->recupererParClePrimaire(rawurldecode($_REQUEST['login']));
        if ($user == null) {
            self::afficherErreur("L'utilisateur n'existe pas");
        }else{
            if (MotDePasse::verifier($_REQUEST["currentmdp"], $user->getMdpHache()) == false) {
                self::afficherErreur("Mot de passe incorrect");
            }else{
                if(VerificationEmail::aValideEmailuser($user)){
                    ConnexionUtilisateur::connecter($user->getLogin());
                    self::afficherVue("../vue/vueGenerale.php",["titre" => "Utilisateur connecté","cheminCorpsVue" => "utilisateur/utilisateurConnecte.php","utilisateur" => $user]);
                }else{
                    self::afficherErreur("Veuillez vérifier votre email");
                }
            }
        }
    }

    public static function deconnecter()
    {
        ConnexionUtilisateur::deconnecter();
        self::afficherVue("../vue/vueGenerale.php",["titre" => "Utilisateur déconnecté","cheminCorpsVue" => "utilisateur/utilisateurDeconnecte.php","utilisateurs" => (new UtilisateurRepository())->recuperer()]);
    }

    /**c
     * @return Utilisateur
     */
    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        $login = $tableauDonneesFormulaire['login'];
        $nom = $tableauDonneesFormulaire['nom'];
        $prenom = $tableauDonneesFormulaire['prenom'];
        $mdpHache = MotDePasse::hacher($tableauDonneesFormulaire['mdp']);
        $estAdmin = isset($tableauDonneesFormulaire['estAdmin']);
        $email = "";
        $emailAValider = $tableauDonneesFormulaire['email'];
        $nonce = MotDePasse::genererChaineAleatoire();
        return new Utilisateur($login, $nom, $prenom, $mdpHache, $estAdmin, $email, $emailAValider, $nonce);
    }

    /*public static function deposerCookie() :void {
        Cookie::enregistrer("Cookie1", [1,2,3,4,5]);
    }
    public static function lireCookie() :void {
        print_r(Cookie::lire("Cookie1"));
    }*/
    public static function supprimerCookie() :void {
        Cookie::supprimer("Cookie1");
    }

    public static function validerEmail (){
        if(isset($_REQUEST["login"]) && isset($_REQUEST["nonce"])){
            if(VerificationEmail::traiterEmailValidation(rawurldecode($_REQUEST["login"]), $_REQUEST["nonce"])){
                self::afficherDetail();
            }else {
                self::afficherErreur("Problème avec la vérification du mail");
            }
        }
    }
//    public static function testSession() :void {
//        $session = Session::getInstance();
//        $session->enregistrer("utilisateur", "Cathy Penneflamme");
//        $session->enregistrer("tableau", [0,1,2,3]);
//        $session->enregistrer("entier", 1);
//        print_r($_SESSION);
//        $session->supprimer("entier");
//        print_r($_SESSION);
//        $session->detruire();
//        print_r($_SESSION);
//    }
}