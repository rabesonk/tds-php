<?php
namespace App\Covoiturage\Modele\Repository;
use App\Covoiturage\Modele\ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Utilisateur;

abstract class AbstractRepository{
    public function mettreAJour(AbstractDataObject $objet): void
    {
        $sql = "UPDATE " . $this->getNomTable() . " SET ";
        $colonnes = $this->getNomsColonnes();
        for ($i = 0; $i < sizeof($colonnes); $i++) {
            if ($i == sizeof($colonnes) - 1) {
                $sql .= $colonnes[$i] . "= :" . $colonnes[$i] . " ";
            } else {
                $sql .= $colonnes[$i] . "= :" . $colonnes[$i] . ", ";
            }
        }
        $sql .= " WHERE " . $this->getNomClePrimaire() . " = :" . $this->getNomClePrimaire();
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($this->formatTableauSQL($objet));
    }

    public function ajouter(AbstractDataObject $objet): void
    {
        $colonnes = join(",",$this->getNomsColonnes());
        $sql = "INSERT INTO " . $this->getNomTable() . " (".$colonnes.") VALUES (:" . join(",:",$this->getNomsColonnes()) . ")";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($this->formatTableauSQL($objet));
    }

    public function supprimer($valeurClePrimaire): void
    {
        $sql = "DELETE FROM ".$this->getNomTable()." WHERE ".$this->getNomClePrimaire()." = :clePrimaire";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "clePrimaire" => $valeurClePrimaire
        );
        $pdoStatement->execute($values);
    }

    public function recupererParClePrimaire(string $valeurClePrimaire): ?AbstractDataObject
    {
        $sql = "SELECT * FROM ".$this->getNomTable()." WHERE ".$this->getNomClePrimaire()." = :clePrimaire";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "clePrimaire" => $valeurClePrimaire,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $utilisateurFormatTableau = $pdoStatement->fetch();
        if (empty($utilisateurFormatTableau)) {
            return null;
        } else {
            return $this->construireDepuisTableauSQL($utilisateurFormatTableau);
        }
    }

    public function recuperer(): array
    {
        $Pdo = \App\Covoiturage\Modele\ConnexionBaseDeDonnees::getPdo();
        $sql = 'SELECT * FROM '.$this->getNomTable();
        $pdoStatement = $Pdo->query($sql);
        $Tabobjets = [];
        foreach ($pdoStatement as $objetFormatTableau) {
            $objet = $this->construireDepuisTableauSQL($objetFormatTableau);
            $Tabobjets[] = $objet;
        }
        return $Tabobjets;
    }
    protected abstract function getNomTable() : string;
    protected abstract function construireDepuisTableauSQL(array $objetFormatTableau) : AbstractDataObject;
    protected abstract function getNomClePrimaire() : string;
    /** @return string[] */
    protected abstract function getNomsColonnes(): array;
    protected abstract function formatTableauSQL(AbstractDataObject $objet): array;
}