<?php
namespace App\Covoiturage\Modele\DataObject;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\ConnexionBaseDeDonnees as ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

/*require_once __DIR__ . '/ConnexionBaseDeDonnees.php';
require_once __DIR__ . '/../../Trajet.php';*/
class Utilisateur extends AbstractDataObject
{

    private string $login;
    private string $nom;
    private string $prenom;
    /**
     * @var Trajet[]|null
     */
    private ?array $trajetsCommePassager;

    private string $mdpHache;

    private bool $estAdmin;

    private string $email;

    private string $emailAValider;

    private string $nonce;

    // un getter
    public function getNom() : string {
        return $this->nom;
    }

    // un setter
    public function setNom($nom) : void {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getPrenom() : string
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom($prenom): void
    {
        $this->prenom = $prenom;
    }

    /**
     * @return mixed
     */
    public function getLogin() : string
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin($login): void
    {
        if (strlen($login) > 64) {
            $this->login = substr($login, 0, 64);
        }else{
            $this->login = $login;
        }
    }

    // un constructeur
    public function __construct(
      string $login,
      string $nom,
      string $prenom,
      string $mdpHache,
      bool $estAdmin,
      string $email,
      string $emailAValider,
      string $nonce) {
        if (strlen($login) > 64) {
            $this->login = substr($login, 0, 65);
        }else{
            $this->login = $login;
        }
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->trajetsCommePassager = null;
        $this->mdpHache = $mdpHache;
        $this->estAdmin = $estAdmin;
        $this->email = $email;
        $this->emailAValider = $emailAValider;
        $this->nonce = $nonce;
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
    /*public function __toString() : string {
      // À compléter dans le prochain exercicere
        return "Utilisateur {$this->prenom} {$this->nom} de login {$this->login}";
    }*/

    public function getTrajetsCommePassager(): ?array
    {
        if(empty($this->trajetsCommePassager)){
            return UtilisateurRepository::recupererTrajetsCommePassager();
        }else{
            return $this->trajetsCommePassager;
        }
    }

    public function setTrajetsCommePassager(?array $trajetsCommePassager): void
    {
        $this->trajetsCommePassager = $trajetsCommePassager;
    }

    public function getMdpHache(): string
    {
        return $this->mdpHache;
    }

    public function setMdpHache(string $mdpHache): void
    {
        $this->mdpHache = $mdpHache;
    }
    public function getEstAdmin(): bool
    {
        return $this->estAdmin;
    }
    public function setEstAdmin(bool $estAdmin): void
    {
        $this->estAdmin = $estAdmin;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getEmailAValider(): string
    {
        return $this->emailAValider;
    }

    public function setEmailAValider(string $emailAValider): void
    {
        $this->emailAValider = $emailAValider;
    }

    public function getNonce(): string
    {
        return $this->nonce;
    }

    public function setNonce(string $nonce): void
    {
        $this->nonce = $nonce;
    }
}