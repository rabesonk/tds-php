<?php

namespace App\Covoiturage\Lib;

use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class ConnexionUtilisateur
{
    // L'utilisateur connecté sera enregistré en session associé à la clé suivante
    private static string $cleConnexion = "_utilisateurConnecte";

    public static function connecter(string $loginUtilisateur): void
    {
        // À compléter
        $cleConnexion = self::$cleConnexion;
        $session = Session::getInstance();
        $session->enregistrer($cleConnexion,$loginUtilisateur);
    }

    public static function estConnecte(): bool
    {
        // À compléter
        $cleConnexion = self::$cleConnexion;
        $session = Session::getInstance();
        return $session->contient($cleConnexion);
    }

    public static function deconnecter(): void
    {
        // À compléter
        $cleConnexion = self::$cleConnexion;
        $session = Session::getInstance();
        $session->supprimer($cleConnexion);
    }

    public static function getLoginUtilisateurConnecte(): ?string
    {
        // À compléter
        return Session::getInstance()->contient(self::$cleConnexion) ? Session::getInstance()->lire(self::$cleConnexion) : null;
    }

    public static function estUtilisateur($login): bool
    {
        if(self::estConnecte()){
            return self::getLoginUtilisateurConnecte() === $login;
        }
        return false;
    }

    public static function estAdministrateur() : bool
    {
        if(self::estConnecte()){
            $user = (new UtilisateurRepository())->recupererParClePrimaire(self::getLoginUtilisateurConnecte());
            return $user->getEstAdmin();
        }else{
            return false;
        }
    }
}